<?php

require_once ('animal.php');
require_once ('frog.php');
require_once ('ape.php');

$sheep = new animal("shaun");
echo "Nama Hewan = " . $sheep->jenis . "<br>" ; // "shaun"
echo "Jumlah Kaki = " . $sheep->legs . "<br>" ; // 4
echo "Hewan Berdarah Dingin =" . $sheep->cold_blooded . "<br><br>"; // "no"

$kodok = new frog("buduk");
echo "Nama Hewan = " . $kodok->jenis . "<br>";
echo "Jumlah Kaki = " . $kodok->legs . "<br>";
echo "Hewan Berdarah Dingin = " . $kodok->cold_blooded . "<br>";
echo "Berjalan = " . $kodok->jump . "<br><br>";

$sungokong = new ape("kera sakti");
echo "Nama Hewan = " . $sungokong->jenis . "<br>";
echo "Jumlah Kaki = " . $sungokong->legs . "<br>";
echo "Hewan Berdarah Dingin = " . $sungokong->cold_blooded . "<br>";
echo "Suara = " . $sungokong->yell . "<br><br";

?>
